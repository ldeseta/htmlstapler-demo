package com.ideasagiles.htmlstapler;

import javax.servlet.http.HttpServletRequest;
import jodd.htmlstapler.HtmlStaplerFilter;
import jodd.io.FileNameUtil;

/**
 *
 * @author Leito
 */
public class MyStaplerFilter extends HtmlStaplerFilter {

    @Override
    protected boolean acceptActionPath(HttpServletRequest request, String actionPath) {
        String extension = FileNameUtil.getExtension(actionPath);
        if (extension.length() == 0) {
            return true;
        }
        if (extension.equals("html") || extension.equals("htm") || extension.equals("jsp")) {
            return true;
        }
        return false;
    }
}
